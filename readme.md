Running dispatch.py
-------------------
1. `python manage.py syncdb` to create the tables. :-)

2. `python manage.py sample <package_name>` will create a quick model, with the same
name as the package settings in the environment variables. Use it for quickly testing if the
script works. 

3. `python manage.py dispatch` will run the `send_all` function in
ptsapp/dispatch.py, which will read mail from stdin and start then
start dispatching emails to appropriate users.

4. Alternatively, `python manage.py dispatch <package_name>` will dispatch the
mail for a particular package.

You may have to use `sudo` depending on the location of the log directory.

You can also run tests using `python manage.py test ptsapp`. :-)
