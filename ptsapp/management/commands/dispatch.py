#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Ref: https://docs.djangoproject.com/en/dev/howto/custom-management-commands/

from django.core.management.base import BaseCommand, CommandError

from ptsapp.dispatch import send_all
from ptsapp.models import Subscription, Bounce

class Command(BaseCommand):
    help = 'Dispatch the emails for relevant packages'

    def handle(self, *args, **kwargs):
        send_all()
