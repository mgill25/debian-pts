"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
import email

from django.test import TestCase
from django.test.utils import setup_test_environment
from django.test.client import Client

from ptsapp.models import Subscription, Tag
from ptsapp.dispatch import *

setup_test_environment()

class SimpleTest(TestCase):
    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)

class DispatchTest(TestCase):
    def test_get_package_name(self):
        os.environ['LOCAL_PART'] = "Foo"
        os.environ['LOCAL_PART_SUFFIX'] = "_BAr"
        self.assertEqual(get_package_name(), "foo_bar")
        del os.environ['LOCAL_PART']
        del os.environ['LOCAL_PART_SUFFIX']

    def test_get_package_keyword(self):
        os.environ['LOCAL_PART'] = "Foo"
        os.environ['LOCAL_PART_SUFFIX'] = "_BAr"
        self.assertEqual(get_package_keyword(), ('foo', 'bar'))
        del os.environ['LOCAL_PART']
        del os.environ['LOCAL_PART_SUFFIX']

    def test_filter_tags_default(self):
        os.environ['LOCAL_PART'] = "Foo"
        os.environ['LOCAL_PART_SUFFIX'] = "default"

        p = Package(name='MyPackage'); p.save()
        s = Subscription.objects.get_or_create(address='hello@debian.org', package=p)[0]
        s.generate_default_tags(); s.save()

        self.assertTrue(filter_tags(s, 'default'))
        self.assertTrue(filter_tags(s, 'bts'))
        self.assertTrue(filter_tags(s, 'bts-control'))
        self.assertTrue(filter_tags(s, 'katie-other'))
        self.assertTrue(filter_tags(s, 'contact'))
        self.assertTrue(filter_tags(s, 'summary'))
        self.assertTrue(filter_tags(s, 'upload-source'))
        self.assertTrue(filter_tags(s, 'buildd'))

        del os.environ['LOCAL_PART']
        del os.environ['LOCAL_PART_SUFFIX']

    def test_parse_email(self):
        message = """From: mgill25@outlook.com

        Test Message."""
        msg = email.message_from_string(message)
        parsed_output = parse_mail(msg)
        self.assertEqual(parsed_output[1], 'no-msgid-present@localhost')
        self.assertEqual(parsed_output[2], 'mgill25@outlook.com')

    def test_parse_email_fail_xloop(self):
        message = """From: mgill25@outlook.com\nX-Loop: python@packages.qa.debian.org

        Test Message.
        """
        msg = email.message_from_string(message)
        self.assertEqual(None, parse_mail(msg))

    def test_inject_headers(self):
        message = """From: mgill25@outlook.com

        Hello, World."""
        msg = email.message_from_string(message)
        injected_output = inject_headers(msg)
        self.assertEqual(injected_output['From'], 'mgill25@outlook.com')
        self.assertEqual(injected_output['X-Loop'], 'python_bts@packages.qa.debian.org')
        self.assertEqual(injected_output['X-Debian'], 'PTS')
        self.assertEqual(injected_output['X-Debian-Package'], 'python_bts')
        self.assertEqual(injected_output['X-PTS-Package'], 'python_bts')
        self.assertEqual(injected_output['X-PTS-Keyword'], 'bts')
        self.assertEqual(injected_output['List-Unsubscribe'], '<mailto:pts@qa.debian.org?body=unsubscribe%20python_bts>')

    def test_filter_tags_address_package(self):
        os.environ['LOCAL_PART'] = 'hello'
        os.environ['LOCAL_PART_SUFFIX'] = 'katie-other'
        p = Package(name='MyPackage'); p.save()
        s = Subscription(address='hello@debian.org', package=p); s.save()
        s.set_tag("summary")
        s.save()
        self.assertTrue(filter_tags(s, "summary"))
        del os.environ['LOCAL_PART']
        del os.environ['LOCAL_PART_SUFFIX']

    def test_update_keyword(self):
        p = Package(name='MyPackage'); p.save()
        s = Subscription.objects.get_or_create(address='hello@debian.org', package=p)[0]
        s.save()
        message = """From: mgill25@outlook.com\nX-Loop: owner@bugs.debian.org\nX-Debian-PR-Message: transcript"""
        msg = email.message_from_string(message)
        self.assertEqual(update_keyword(msg), 'bts-control')

        message = """From: mgill25@outlook.com\nX-Loop: owner@bugs.debian.org\nX-Debian-PR-Message: Heyo!"""
        msg = email.message_from_string(message)
        self.assertEqual(update_keyword(msg), 'bts')

        message = """From: mgill25@outlook.com\nSubject: Accepted\nX-DAK: Foo\n
        Source Package URL: http://cdn.debian.net/debian/pool/main/f/figlet/figlet_2.2.2-1.dsc"""
        msg = email.message_from_string(message)
        self.assertEqual(update_keyword(msg), 'upload-source')

        message = """From: mgill25@outlook.com\nSubject: INSTALLED\nX-DAK: Foo\n
        Binary Package Installed!!!"""
        msg = email.message_from_string(message)
        self.assertEqual(update_keyword(msg), 'upload-binary')

        message = """From: mgill25@outlook.com\nSubject: Comments Regarding MyPackage.changes\nX-DAK: Foo\n
        Binary Package Installed!!!"""
        msg = email.message_from_string(message)
        self.assertEqual(update_keyword(msg), 'katie-other')



